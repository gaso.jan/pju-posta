# PJU Assignment
​
## Uvod
​
Naloga zahteva poznavanje frontend razvoja s poljubnim frontend ogrodjem (plus točke, za uporabo Vue.js 😄) ter delo z git orodjem.
Ustvari preprost checkout spletne trgovine, na katerem mora uporabnik preko pojavnega okna izbrati lokacijo dostave (paketomat, pošta).
​
## 'Pravila' git
​
Ustvari nov branch in po končanem delu ustvari pull request za merge v main vejo.
​
## Zahteve UI/UX
​
Mockup oz. wireframe izgleda vmesnika je dostopen [TUKAJ.](https://www.figma.com/file/YGi9JKJ92mW3tkEzVRk9hE/pju-assignment?node-id=0%3A1)
​
Checkout mora vsebovati:
​
-   Polja za vnos (ime, priimek, telefonska številka, e-pošta)
-   Izbiro paketomata/pošte z uporabniškim vmesnikom češke pošte.
- 	Izbiro plačilne metode (po povzetju in s kreditno kartico)
-   Gumb za potrditev naročila, če so polja uztrezno izpolnjena. (validacija polj + POST request)
​
## Delovanje izbire paketomata/pošte
​
Izgled mora biti narejen po predpisu 2. artboarda na [mockupu](https://www.figma.com/file/YGi9JKJ92mW3tkEzVRk9hE/pju-assignment?node-id=0%3A1) (pojavno okno). Izbira paketomata poteka z `iframe` html elementom v pojavnem oknu.
​
Vir `iframe` elementa je [vmesnik češke pošte](https://b2c.cpost.cz/locations/). Url:
​
```
https://b2c.cpost.cz/locations/
```
​
Po uspešni izbiri pošte na zemljevidu, vnosu telefonske številke in potrditvi z pritiskom na gumb 'Vyzvednout zde', na frontend prejmemo `json` podatke o lokaciji prevzemnega meste preko 'eventlistenerja':
`Window.postMessage()`
​
Dokumentacijo lahko najdeš [TUKAJ.](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
​
Če misliš, da bi izbiro paketomata lahko implementirali drugače, lahko frontend tudi spremeniš, ter spremembe utemeljiš.
​
## POST
​
Podatke, pošlješ na strežnik preko http zahtevka tipa POST na endpoint:
​
```
https://backsfit.com/pju-assignment/order/create
```
​
Body vsebina mora biti sledeča:
​
-   primer body vsebine
​
```json
{
	"email": "test@test.com",
	"phone": "051665123",
	"first_name": "Test",
	"last_name": "Test",
	"address": "Testna ulica 123",
	"postcode": 1000,
	"products": ["123"], // ostane ista vrednost
	"country": "CZ", // ostane ista vrednost,
	"payment_method": "cod",
	"delivery_type": "BALIKOVNY",
	"office_data": {
		"id": 233183,
		"name": "Nučice"
	}
}
```

Pri payment_method parametru sta možni vrednosti dve; cod - v primeru plačila po povzetju, credit_card - v primeru plačila s kartico.
​
Potrebno je paziti tudi na tipe poslanih podatkov.

## Error handling

V primeru napake v podatkih bo v sklop napake prišel podatek o HTTP statusu, z njim pa tudi obrazložitev statusa, ki jo lahko najdeš v `response.error.data.message`.
​
# Priporočila
​
Kodo uztrezno dokumentiraj oz. predstavi delovanje funkcij/implementacije tam, kjer je to potrebno.
